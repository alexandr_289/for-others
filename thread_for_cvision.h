#ifndef THREAD_FOR_CVISION_H
#define THREAD_FOR_CVISION_H

#define BUF_LEN 65540
#define PACK_SIZE 4096
#define HOST_PORT 5000
//#define IP "192.168.0.96"//user IP
#define IP "127.0.0.1"

#include <arpa/inet.h>
#include <iostream>
#include <QString>
#include "opencv2/opencv.hpp"
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <QThread>
#include "thread_for_map.h"

using namespace std;
using namespace cv;




class thread_for_CVision : public QThread {
public:
    void run(/*int host_port, const char *IP*/);
    void drawing_map(Mat* frame, int l, int w, int x, int y);
    void build_thread_map(int &ref_x, int &ref_y, int &bytes_read);
protected:
    virtual void keyPressEvent(QKeyEvent *event);
};

#endif // THREAD_FOR_CVISION_H
