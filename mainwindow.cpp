#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "my_client.h"
#include "thread_for_cvision.h"
#include <QLabel>
#include <QPushButton>
#include <QEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    /*Создание анимации */
    QMovie *movie = new QMovie("file.gif");
    QLabel *processLabel = new QLabel(this);
    processLabel->setGeometry(0,0,2900,700);
    processLabel->setMovie(movie);

    /* Создание кнопок */
    QPushButton *start_game = new QPushButton(processLabel);
    start_game->setText(QString("Начать игру"));
    start_game->setStyleSheet(
                "QPushButton{"
                "background-color: rgb(80, 80, 80);" // Фон изначально
                "border-radius: 5px;"
                "border-bottom: 3px transparent;"
                "border-right: 2px transparent;"
                "border-left: 2px transparent;}"
                "QPushButton:hover{"
                "background-color: rgb(84,14,173);"// Цвет при наведении курсора
                "color: rgb(238, 230, 247);} "
                "QPushButton:pressed  {"
                "background-color: rgb(75,15, 150); } " // Цвет при нажатии
                );

    start_game->setGeometry(50,500,110,30);
    QObject::connect(start_game, SIGNAL(clicked()), SLOT(start_new_game()));



    QPushButton *settings = new QPushButton(processLabel);
    settings->setText(QString("Настройки"));

    settings->setStyleSheet(
                "QPushButton{"
                "background-color: rgb(80, 80, 80);"
                "border-radius: 5px;"
                "border-bottom: 3px transparent;"
                "border-right: 2px transparent;"
                "border-left: 2px transparent;}"
                "QPushButton:hover{"
                "background-color: rgb(84,14,173);"
                "color: rgb(238, 230, 247);} "
                "QPushButton:pressed  {"
                "background-color: rgb(75,15, 150); } "
                );


    settings->setGeometry(50,550,110,30);


    QPushButton *exit = new QPushButton(processLabel);
    exit->setText(QString("Выход"));

    exit->setStyleSheet(
                "QPushButton{"
                "background-color: rgb(80,80,80);"
                "border-radius: 5px;"
                "border-bottom: 3px transparent;"
                "border-right: 2px transparent;"
                "border-left: 2px transparent;}"
                "QPushButton:hover{"
                "background-color: rgb(84,14,173);"
                "color: rgb(238, 230, 247);} "
                "QPushButton:pressed  {"
                "background-color: rgb(75,15, 150); } "
                );


    exit->setGeometry(50,600,110,30);
    QObject::connect(exit, SIGNAL(clicked()), SLOT(close()));

    /*Размеры для анимации (лучше не трогать) */
    movie->setScaledSize(QSize(1000,1000));
    movie->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}





void MainWindow::start_new_game()
{
    this->hide();



    static thread_for_CVision CVision;
    CVision.start();

    // static my_client client("192.168.0.26",4000); //Robot IP
    static my_client client("localhost",4000);
    client.show();
  //  client.setWindowState( (windowState() & ~Qt::WindowMinimized) | Qt::WindowActive); // Окно теперь "главное"
}




