#ifndef MY_CLIENT_H
#define MY_CLIENT_H

#include <QWidget>
#include <QTcpSocket>

class QTextEdit;
class QLineEdit;

class my_client : public QWidget {
    Q_OBJECT
private:
    QTcpSocket *m_pTcpSocket; // Для управления клиентом
    QTextEdit *m_ptxtInfo;  // Для вывода информации
    QLineEdit *m_ptxtInput; // Для ввода информации
    quint16 m_nNextBlockSize; //Для хранения длины следующего блока сокета
public:
    my_client(const QString &strHost, int nPort, QWidget *pwgt = nullptr);
private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
    void slotSendToServer();
    void slotConnected();
signals:
protected:
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
};

#endif // MY_CLIENT_H

