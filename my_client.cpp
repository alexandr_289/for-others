#include <QTcpServer>
#include <QMessageBox>
#include <QTextEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QtWidgets>
#include "my_client.h"
#include <QKeyEvent>
#include "unistd.h"
#include <QDebug>
int forwardIs, backIs, leftIs, rightIs, boostIs;

/* Реализация конструктора */

my_client::my_client(const QString &strHost, int nPort, QWidget *pwgt ) : QWidget(pwgt), m_nNextBlockSize(0) {

    m_pTcpSocket = new QTcpSocket(this); // Создание объекта сокета

    m_pTcpSocket->connectToHost(strHost, nPort); // Связь с сервером, strHost - имя компьютера, nPort - номер порта

    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected())); // Высылает сигнал как только произведено соединение
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead())); // Сигнал о готовности предоставить данные для чтения
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError))); // Отобр.ошибок

     // Cозданиеинтерфейса

    m_ptxtInfo  = new QTextEdit;
    m_ptxtInfo->setReadOnly(true);

    //Layout setup
    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(new QLabel("<H1>Game</H1>"));
    pvbxLayout->addWidget(m_ptxtInfo);
    setLayout(pvbxLayout);


    /* Делает окно прозрачным */
   /* setWindowFlags(Qt::Widget | Qt::FramelessWindowHint);
    setParent(0); // Create TopLevel-Widget
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_TranslucentBackground, true);
    setAttribute(Qt::WA_PaintOnScreen);*/


}

    /* Этот слот вызывается при поступлении данных от сервера; цикл for, т.к. не все данные могут прийти одновременно */
void my_client::slotReadyRead(){


    QDataStream in(m_pTcpSocket);
    in.setVersion(QDataStream::Qt_4_2);
    for (;;) {
        if(!m_nNextBlockSize){
            if(m_pTcpSocket->bytesAvailable() < sizeof(quint16)){
                break;
            }
            in >> m_nNextBlockSize;
        }
        if(m_pTcpSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }
        QTime time;
        QString str;
        in >> time >> str;

        /* Полученная информация добавляется в виджет многострочного текстового поля (указатель m_ptxtInfo) с помощью метода append() */
        m_ptxtInfo->append(time.toString() + " " + str);
        m_nNextBlockSize = 0; // Указывает на то, что размер очередного блока неизвестен
    }
}

void my_client::slotError(QAbstractSocket::SocketError err) {
    QString strError = "Error: " + (err == QAbstractSocket::HostNotFoundError ? "The host was not found." :
                                                                                err == QAbstractSocket::RemoteHostClosedError ?
                                                                                "The remote host is closed." :
                                                                                err == QAbstractSocket::ConnectionRefusedError ?
                                                                                "The connection was refused." :
                                                                                QString(m_pTcpSocket->errorString())
                                                                               );
    m_ptxtInfo->append(strError);
}




/* После нажатия кнопки Send вызывается слот slotSendToServer()*/

void my_client::keyPressEvent(QKeyEvent *event){
    if(event->isAutoRepeat())
    {
        return;
    }
    switch (event->key()){
    case Qt::Key_W:
        forwardIs = 1;
        break;
    case Qt::Key_S:
        backIs = 1;
        break;
    case Qt::Key_A:
        leftIs = 1;
        break;
    case Qt::Key_D:
        rightIs = 1;
        break;
    default:
        break;
    }
    slotSendToServer();
}

void my_client::keyReleaseEvent(QKeyEvent *event){
    if(event->isAutoRepeat())
    {
        return;
    }
    switch (event->key()){
    case Qt::Key_W:
        forwardIs = 0;
        break;
   case Qt::Key_S:
        backIs = 0;
        break;
    case Qt::Key_A:
        leftIs = 0;
        break;
    case Qt::Key_D:
        rightIs = 0;
        break;
    default:
        break;
    }

    slotSendToServer();
}


void my_client::slotSendToServer() {
        char arrBlock[128];
        QString str = "0000";

        str[0] = rightIs + '0';
        str[1] = leftIs + '0';
        str[2] = backIs + '0';
        str[3] = forwardIs + '0';
        strcpy(arrBlock, str.toUtf8().constData());
        m_pTcpSocket->write(arrBlock);
        m_ptxtInfo->append(arrBlock);
         qDebug() << str[0] <<  str[1] << str[2] << str[3];

}

/* Как только связь с сервером установлена, вызывается метод slotConnected() и в виджет текстового поля добавляется строка сообщения */
void my_client::slotConnected(){
    m_ptxtInfo->append("Received the connected() signal");
}
